<%--
  Created by IntelliJ IDEA.
  User: aavanderleij
  Date: 28-11-17
  Time: 13:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Users</title>
</head>
<body>

    <h2>user: ${requestScope.userOne.userName}</h2>
    <h2>pass: ${requestScope.userOne.passWord}</h2>
    <h2>address: ${requestScope.userOne.address}</h2>
    <h2>email: ${requestScope.userOne.email}</h2>
    <h2>street: ${requestScope.userOne.address.steed}</h2>
    <h2>number: ${requestScope.userOne.address.number}</h2>

    <h1>All users</h1>
    <table>
        <thead>
        <tr>
            <th>name</th>
            <th>email</th>
            <th>addres</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="person" items="${requestScope.registerd_users}">
            <tr>
                <td>${person.userName}</td>
                <td>${person.email}</td>
                <td>${person.address.getPrinttableAddress}</td>
            </tr>
        </c:forEach>
        </c>
        </tbody>
    </table>

    <jsp:include page="includes/footer.jsp">
        <jsp:param name="todays_saying" value="beter een dode kakkerlak in de tuin dan een levende"></jsp:param>
    </jsp:include>


</body>
</html>
