<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: aavanderleij
  Date: 23-11-17
  Time: 15:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>SuperSecret</title>
</head>
<body>

<c:choose>
    <c:when test="${sessionScope.user != null}">
        <h1>"Awesome, you made it to here ${requestScope.user}!"</h1>
        <h3>please have al look at my ripped anime collection.</h3>
    </c:when>
    <c:otherwise>
        <h2>please log in first</h2>
        <c:redirect url="/login.do"/>
    </c:otherwise>
</c:choose>


</body>
</html>
