package nl.bioinf.aavanderleij.users;

public class Address {
    private int number;
    private String numberPostFix;
    private String zipCode;
    private String steed;
    private String city;

    public Address(int number, String numberPostFix, String zipCode, String streed, String city) {
        this.number = number;
        this.numberPostFix = numberPostFix;
        this.zipCode = zipCode;
        this.steed = streed;
        this.city = city;
    }

    public Address(int number, String zipCode, String street, String city) {
        this(number, null, zipCode, street, city);
    }

    public int getNumber() {
        return number;
    }

    public String getNumberPostFix() {
        return numberPostFix;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getSteed() {
        return steed;
    }

    public String getCity() {
        return city;
    }

    public String getPrinttableAddress(){
        return getSteed() + getNumber() + getZipCode() + getCity();
    }

    @Override
    public String toString() {
        return "Address{" +
                "number=" + number +
                ", numberPostFix='" + numberPostFix + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", steed='" + steed + '\'' +
                ", city='" + city + '\'' +
                '}';
    }


}
