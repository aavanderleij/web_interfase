package nl.bioinf.aavanderleij.webbased_thema10.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;

@WebServlet(name = "helloServlet", urlPatterns = "/hallo")
public class helloServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LocalDateTime time = LocalDateTime.now();
        String name = "Antsje";
        request.setAttribute("time", time.toString());
        request.setAttribute("name", name);
        RequestDispatcher view = request.getRequestDispatcher("hallo.jsp");
        view.forward(request,response);
//        PrintWriter out = response.getWriter();

//        out.write( "<html><head><title>MyFirstServlet</title></head><body><h1>hello, it is now</h1></body></html>" + time.toString());

    }
}
