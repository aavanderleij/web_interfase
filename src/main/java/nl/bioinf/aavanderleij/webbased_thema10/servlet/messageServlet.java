package nl.bioinf.aavanderleij.webbased_thema10.servlet;

import nl.bioinf.aavanderleij.thema10.messeges.MessageFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "messageServlet", urlPatterns = "/message")
public class messageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = MessageFactory.getMessage();
        request.setAttribute("message", message);
        RequestDispatcher view = request.getRequestDispatcher("message.jsp");
        view.forward(request,response);

    }
}
