package nl.bioinf.aavanderleij.webbased_thema10.servlet;

import nl.bioinf.aavanderleij.users.Address;
import nl.bioinf.aavanderleij.users.Users;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "userServlet",urlPatterns = "/list.users")
public class userListingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Users numberOne = createUserOne();
        request.setAttribute("userOne", numberOne);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("UserListing.jsp");
        requestDispatcher.forward(request,response);



        List<Users> registerdUsers = getRegisterdUsers();
        request.setAttribute("registerd_users", registerdUsers);
    }

    private Users createUserOne() {
        Users one = new Users ("Henk","Henk");
        one.setEmail("Henk@example.com");
        Address address = new Address(7 ,"a", "9701 DA", "Zernikeplein", "Gronignen");
        one.setAddress(address);
        return one;
    }

    public ArrayList<Users> getRegisterdUsers(){
        ArrayList<Users> users = new ArrayList<>();

        Users u = new Users ("Henk","Henk");
        u.setEmail("Henk@example.com");
        Address address = new Address(7 ,"a", "9701 DA", "Zernikeplein", "Gronignen");
        u.setAddress(address);
        users.add(u);

        u = new Users ("Piet","Piet");
        u.setEmail("Piet@example.com");
        address = new Address(8 ,"a", "9701 DA", "Zernikeplein", "Gronignen");
        u.setAddress(address);
        users.add(u);

        u = new Users ("Roos","Roos");
        u.setEmail("Roos@example.com");
        address = new Address(17 , "9701 DA", "Zernikeplein", "Gronignen");
        u.setAddress(address);
        users.add(u);

        return users;





    }
}
